package rw.bk.notificationservice.routes

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.reactive.function.server.RequestPredicates.accept
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.RouterFunctions.route
import org.springframework.web.reactive.function.server.ServerResponse
import rw.bk.notificationservice.exceptions.ErrorResponsePayload
import rw.bk.notificationservice.models.NotificationMessagesTemplates
import rw.bk.notificationservice.routes.handlers.NotificationManagementHandler
import rw.bk.notificationservice.sevices.NotificationManagementService


@Configuration
class NotificationManagementRoutes {

    @Bean
    @RouterOperations(
        RouterOperation(
            path = "/notifications-templates",
            beanClass = NotificationManagementService::class,
            beanMethod = "addTemplates",
            method = arrayOf(RequestMethod.POST),
            operation = Operation(
                operationId = "addTemplates",
                summary = "Add Templates of Messages",
                tags = ["Management"],
                responses = [ApiResponse(
                    responseCode = "200",
                    description = "successful operation",
                    content = [Content(schema = Schema(implementation = NotificationMessagesTemplates::class))]
                ), ApiResponse(
                    responseCode = "400",
                    description = "Invalid parameters",
                    content = [Content(schema = Schema(implementation = ErrorResponsePayload::class))]
                )]
            )
        ), RouterOperation(
            path = "/notifications-templates",
            beanClass = NotificationManagementService::class,
            beanMethod = "getAllTemplates",
            method = arrayOf(RequestMethod.GET),
            operation = Operation(
                operationId = "getAllTemplates",
                summary = "Get all Templates of Messages",
                tags = ["Management"],
                responses = [ApiResponse(
                    responseCode = "200",
                    description = "successful operation",
                    content = [Content(array = ArraySchema(schema = Schema(implementation = NotificationMessagesTemplates::class)))]
                ), ApiResponse(
                    responseCode = "500",
                    description = "Server Error",
                    content = [Content(schema = Schema(implementation = ErrorResponsePayload::class))]
                )]
            )
        ), RouterOperation(
            path = "/notifications-templates/{messageId}",
            beanClass = NotificationManagementService::class,
            beanMethod = "getByMessageId",
            method = arrayOf(RequestMethod.GET),
            operation = Operation(
                operationId = "getByMessageId",
                summary = "Get all Templates of Messages",
                parameters = [Parameter(`in` = ParameterIn.PATH, name = "messageId")],
                tags = ["Management"],
                responses = [ApiResponse(
                    responseCode = "200",
                    description = "successful operation",
                    content = [Content(schema = Schema(implementation = NotificationMessagesTemplates::class))]
                ), ApiResponse(
                    responseCode = "500",
                    description = "Server Error",
                    content = [Content(schema = Schema(implementation = ErrorResponsePayload::class))]
                )]
            )
        ), RouterOperation(
            path = "/notifications-templates/{id}",
            beanClass = NotificationManagementService::class,
            beanMethod = "updateTemplates",
            method = arrayOf(RequestMethod.PUT),
            operation = Operation(
                operationId = "updateTemplates",
                summary = "Update Templates of Messages",
                tags = ["Management"],
                responses = [ApiResponse(
                    responseCode = "200",
                    description = "successful operation",
                    content = [Content(schema = Schema(implementation = NotificationMessagesTemplates::class))]
                ), ApiResponse(
                    responseCode = "500",
                    description = "Server Error",
                    content = [Content(schema = Schema(implementation = ErrorResponsePayload::class))]
                )]
            )
        )
    )
    fun managementRoutes(notificationManagementHandler: NotificationManagementHandler): RouterFunction<ServerResponse> {

        return route().path("/notifications-templates") { pathBuilder ->
            pathBuilder.nest(accept(MediaType.APPLICATION_JSON)) { mediaTypeBuilder ->
                mediaTypeBuilder.GET("/{messageId}", notificationManagementHandler::getByMessageId)
                    .GET(notificationManagementHandler::getAllTemplates)
                    .GET("/templates/{id}", notificationManagementHandler::updateTemplates)
                    .POST(notificationManagementHandler::addTemplates)
                    .PUT("/templates/{id}", notificationManagementHandler::updateTemplates)
            }
        }.build()

    }
}