package rw.bk.notificationservice.routes.handlers

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import rw.bk.notificationservice.exceptions.BadRequestException
import rw.bk.notificationservice.helpers.BodyValidator
import rw.bk.notificationservice.models.NotificationMessagesTemplates
import rw.bk.notificationservice.models.requests.NotificationTemplatesRequest
import rw.bk.notificationservice.sevices.NotificationManagementService
import java.util.*

@Component
class NotificationManagementHandler(
    private var bodyValidator: BodyValidator,
    private var notificationManagementService: NotificationManagementService
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun addTemplates(serverRequest: ServerRequest): Mono<ServerResponse> {
        return bodyValidator.withValidBody(
            { repayLoanRequest ->

                repayLoanRequest.flatMap {
                    ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                        .body(
                            BodyInserters.fromProducer(
                                notificationManagementService.addTemplates(it),
                                NotificationMessagesTemplates::class.java
                            )
                        )
                }

            }, serverRequest, NotificationTemplatesRequest::class.java
        ).onErrorMap {
            run {
                logger.error(" ${it.printStackTrace()}")
                if (it.localizedMessage.contains("InvalidFormatException"))
                    throw BadRequestException("INVALID_FORMAT")
                else if (it.localizedMessage.contains("ValueInstantiationException"))
                    throw BadRequestException("PARAMETER_MISSING")
                else
                    throw it
            }

        }
    }

    fun getAllTemplates(serverRequest: ServerRequest): Mono<ServerResponse> {
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
            .body(
                BodyInserters.fromProducer(
                    notificationManagementService.getAllTemplates(),
                    NotificationMessagesTemplates::class.java
                )
            )

    }

    fun getByMessageId(serverRequest: ServerRequest): Mono<ServerResponse> {
        val messageId = serverRequest.pathVariable("messageId")
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
            .body(
                BodyInserters.fromProducer(
                    notificationManagementService.getByMessageId(messageId),
                    NotificationMessagesTemplates::class.java
                )
            )
    }

    fun updateTemplates(serverRequest: ServerRequest): Mono<ServerResponse>{
        val templateId = serverRequest.pathVariable("id")
        return bodyValidator.withValidBody(
            { repayLoanRequest ->

                repayLoanRequest.flatMap {
                    ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                        .body(
                            BodyInserters.fromProducer(
                                notificationManagementService.updateTemplates(UUID.fromString(templateId),it),
                                NotificationMessagesTemplates::class.java
                            )
                        )
                }

            }, serverRequest, NotificationTemplatesRequest::class.java
        ).onErrorMap {
            run {
                logger.error(" ${it.printStackTrace()}")
                if (it.localizedMessage.contains("InvalidFormatException"))
                    throw BadRequestException("INVALID_FORMAT")
                else if (it.localizedMessage.contains("ValueInstantiationException"))
                    throw BadRequestException("PARAMETER_MISSING")
                else
                    throw it
            }

        }
    }
    fun getById(serverRequest: ServerRequest): Mono<ServerResponse> {
        val id = serverRequest.pathVariable("id")
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
            .body(
                BodyInserters.fromProducer(
                    notificationManagementService.getById(UUID.fromString(id)),
                    NotificationMessagesTemplates::class.java
                )
            )
    }
}
