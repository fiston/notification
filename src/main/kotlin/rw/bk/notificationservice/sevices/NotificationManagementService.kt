package rw.bk.notificationservice.sevices

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.cache.annotation.CacheConfig
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.bk.notificationservice.exceptions.DuplicateReferenceException
import rw.bk.notificationservice.exceptions.NotFoundException
import rw.bk.notificationservice.models.MessageTranslation
import rw.bk.notificationservice.models.NotificationMessagesTemplates
import rw.bk.notificationservice.models.requests.NotificationTemplatesRequest
import rw.bk.notificationservice.repository.MessageTranslationRepository
import rw.bk.notificationservice.repository.NotificationMessagesTemplatesRepository
import java.util.*


@Service
interface NotificationManagementService {

    fun addTemplates(@RequestBody notificationTemplatesRequest: NotificationTemplatesRequest): Mono<NotificationMessagesTemplates>

    fun getAllTemplates(): Flux<NotificationMessagesTemplates>

    fun getByMessageId(messageId: String): Mono<NotificationMessagesTemplates>

    fun getById(id: UUID): Mono<NotificationMessagesTemplates>

    fun updateTemplates(
        @Parameter(name = "id", `in` = ParameterIn.PATH) templateId: UUID,
        @RequestBody notificationTemplatesRequest: NotificationTemplatesRequest
    ): Mono<NotificationMessagesTemplates>

}

@Service
@CacheConfig(cacheNames= ["Notifications"])
class NotificationManagementServiceImpl(
    private var notificationMessagesTemplatesRepository: NotificationMessagesTemplatesRepository,
    private var messageTranslationRepository: MessageTranslationRepository
) : NotificationManagementService {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private fun saveNotificationMessageTemplates(messageId: String): Mono<NotificationMessagesTemplates> {

        return notificationMessagesTemplatesRepository.findByMessageId(messageId)
            .flatMap { Mono.error<NotificationMessagesTemplates>(DuplicateReferenceException("MESSAGE_ID_ALREADY_EXIST")) }
            .switchIfEmpty(
                notificationMessagesTemplatesRepository.save(
                    NotificationMessagesTemplates(
                        messageId = messageId
                    )
                )
            )
    }
    override fun addTemplates(notificationTemplatesRequest: NotificationTemplatesRequest): Mono<NotificationMessagesTemplates> {

        return saveNotificationMessageTemplates(notificationTemplatesRequest.messageId)
            .onErrorMap { error -> throw error }
            .flatMap { savedNotificationMessagesTemplates ->

                val messageTranslationEntities = notificationTemplatesRequest.translations.map {
                    MessageTranslation(languages = it.language, messageContent = it.translation)
                }
                Flux.fromIterable(messageTranslationEntities)
                    .map { it.copy(notificationMessagesTemplatesId = savedNotificationMessagesTemplates.id!!) } // associate the saved NotificationMessagesTemplates with each MessageTranslation entity
                    .flatMap {
                        logger.info("Saving message translation $it")
                        messageTranslationRepository.save(it)
                    }
                    .collectList()
                    .map { savedMessageTranslations ->
                        logger.info("\n============= Saved message translation $savedMessageTranslations")

                        savedNotificationMessagesTemplates.translations = savedMessageTranslations.toSet()

                        savedNotificationMessagesTemplates
                    }
                    .flatMap { Mono.just(it) }
            }
    }

    override fun getAllTemplates(): Flux<NotificationMessagesTemplates> {

        return notificationMessagesTemplatesRepository.findAll()
            .flatMap { notificationMessagesTemplates: NotificationMessagesTemplates ->
                messageTranslationRepository.findByNotificationMessagesTemplatesId(notificationMessagesTemplates.getId())
                    .collectList()
                    .map {
                        notificationMessagesTemplates.translations = it.toSet()
                        notificationMessagesTemplates
                    }
            }
            .onErrorResume { Flux.empty() }
    }
    @Cacheable
    override fun getByMessageId(messageId: String): Mono<NotificationMessagesTemplates> {
        return notificationMessagesTemplatesRepository.findByMessageId(messageId)
            .flatMap { notificationMessagesTemplates: NotificationMessagesTemplates ->
                messageTranslationRepository.findByNotificationMessagesTemplatesId(notificationMessagesTemplates.getId())
                    .collectList()
                    .map {
                        notificationMessagesTemplates.translations = it.toSet()
                        notificationMessagesTemplates
                    }
            }
            .onErrorResume { Mono.error(NotFoundException("MESSAGE_ID_NOT_FOUND: $messageId")) }
    }

    override fun getById(id: UUID): Mono<NotificationMessagesTemplates> {
        return notificationMessagesTemplatesRepository.findById(id)
            .flatMap { notificationMessagesTemplates: NotificationMessagesTemplates ->
                messageTranslationRepository.findByNotificationMessagesTemplatesId(notificationMessagesTemplates.getId())
                    .collectList()
                    .map {
                        notificationMessagesTemplates.translations = it.toSet()
                        notificationMessagesTemplates
                    }
            }
            .onErrorResume { Mono.error(NotFoundException("MESSAGE_ID_NOT_FOUND: $id")) }
    }


    override fun updateTemplates(
        templateId: UUID,
        notificationTemplatesRequest: NotificationTemplatesRequest
    ): Mono<NotificationMessagesTemplates> {
        return notificationMessagesTemplatesRepository.findById(templateId)
            .flatMap {
                notificationMessagesTemplatesRepository.save(
                    it.apply { messageId = notificationTemplatesRequest.messageId }
                )
            }.flatMap { updatedNotificationMessagesTemplates ->

                messageTranslationRepository.deleteAllByNotificationMessagesTemplatesId(templateId)

                val messageTranslationEntities = notificationTemplatesRequest.translations.map {
                    MessageTranslation(languages = it.language, messageContent = it.translation)
                }
                Flux.fromIterable(messageTranslationEntities)
                    .map { it.copy(notificationMessagesTemplatesId = templateId) } // associate the saved NotificationMessagesTemplates with each MessageTranslation entity
                    .flatMap {
                        logger.info("Saving message translation $it")
                        messageTranslationRepository.save(it)
                    }
                    .collectList()
                    .map { savedMessageTranslations ->
                        logger.info("\n============= Updated message translation $savedMessageTranslations")

                        updatedNotificationMessagesTemplates.translations = savedMessageTranslations.toSet()

                        updatedNotificationMessagesTemplates
                    }
                    .flatMap { Mono.just(it) }
            }
            .switchIfEmpty(Mono.error(NotFoundException("MESSAGE_ID_DOES_NOT_EXIST")))

    }
}
