package rw.bk.notificationservice.sevices

import org.springframework.data.redis.core.ReactiveRedisTemplate
import reactor.core.publisher.Mono
import rw.bk.notificationservice.models.MessageTranslation
import rw.bk.notificationservice.models.NotificationMessagesTemplates
import rw.bk.notificationservice.repository.MessageTranslationRepository
import rw.bk.notificationservice.repository.NotificationMessagesTemplatesRepository

interface MessageTranslationService{
    fun saveNewTranslation():Mono<NotificationMessagesTemplates>
}
class MessageTranslationServiceImpl(
    private val reactiveRedisTemplate: ReactiveRedisTemplate<String, MessageTranslation>,
    private val messageTranslationRepository: MessageTranslationRepository,
    private val notificationMessagesTemplatesRepository: NotificationMessagesTemplatesRepository
) {

    val templatesList = notificationMessagesTemplatesRepository.findAll()



}