package rw.bk.notificationservice.sevices.listeners

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.Acknowledgment
import org.springframework.stereotype.Component
import rw.bk.notificationservice.configuration.ApplicationProperties


@Component
class NotificationListener (private var applicationProperties: ApplicationProperties){

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @KafkaListener(
        topics = ["\${integrations.kafka.topic.notification}"], concurrency = "2",
        groupId = "\${integrations.kafka.group-id.notification}",
        containerFactory = "notificationKafkaListenerContainerFactory",
        autoStartup = "true"
    )
    fun makePayment(consumerRecord: ConsumerRecord<String, Any>, acknowledgment: Acknowledgment) {

//        val messageRepaymentTransaction: NotificationMessagesTemplates = consumerRecord.value() as NotificationMessagesTemplates

        logger.info("==========  ${consumerRecord.topic()}")

        logger.info("========== Preparing to send notification for {}", consumerRecord)


        acknowledgment.acknowledge()
    }
}