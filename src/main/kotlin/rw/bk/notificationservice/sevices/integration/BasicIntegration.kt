package rw.bk.notificationservice.sevices.integration

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.WebClientResponseException
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.bk.notificationservice.exceptions.BadRequestException
import rw.bk.notificationservice.exceptions.DuplicateErrorMapping
import rw.bk.notificationservice.exceptions.InternalServerErrorException
import rw.bk.notificationservice.exceptions.NotFoundException
import rw.bk.notificationservice.helpers.ApiServiceFilters
import rw.bk.notificationservice.helpers.ServiceInstanceFactory
import rw.bk.notificationservice.models.response.ErrorResponse
import java.time.Duration
import java.util.concurrent.TimeoutException

abstract class BasicIntegration(open var baseUrl: String) {

    @Autowired
    lateinit var serviceInstanceFactory: ServiceInstanceFactory

    @Value("\${spring.application.name}")
    private lateinit var appName: String

    @Value("\${integrations.cbss.timeout}")
    lateinit var timeout: String

    lateinit var webClient: WebClient

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun initiator() {
        this.webClient = WebClient.builder()
            .filter(ApiServiceFilters.log(appName))
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .defaultHeader(HttpHeaders.USER_AGENT, appName)
            .build()

    }

    inline fun <reified TRequest, reified TResponse> postToCBSS(
        route: String,
        request: TRequest?
    ): Mono<TResponse?> {
        initiator()
        return serviceInstanceFactory.getLoadBalancedUrl(baseUrl)
            .flatMap {
                try {
                    webClient
                        .post()
                        .uri("$it/$route")
                        .body(if (request == null) Mono.empty<Void>() else Mono.just(request), TRequest::class.java)
                        .retrieve()
                        .onStatus({ httpStatus -> HttpStatus.BAD_REQUEST == httpStatus },
                            { clientResponse: ClientResponse ->
                                clientResponse.bodyToMono(ErrorResponse::class.java)
                                    .onErrorMap { error -> throw BadRequestException(error.localizedMessage) }
                                    .flatMap {cbsErrorResponse->
                                        logger.error("$cbsErrorResponse")
                                        return@flatMap Mono.error(BadRequestException( cbsErrorResponse.message))
                                    }

                            })
                        .onStatus({ httpStatus -> HttpStatus.CONFLICT == httpStatus },
                            { clientResponse: ClientResponse ->
                                clientResponse.bodyToMono(ErrorResponse::class.java)
                                    .onErrorMap { error -> throw BadRequestException(error.localizedMessage) }
                                    .flatMap {cbsErrorResponse->
                                        return@flatMap Mono.error(DuplicateErrorMapping("409", cbsErrorResponse.message))
                                    }

                            })
                        .onStatus({ httpStatus -> HttpStatus.REQUEST_TIMEOUT == httpStatus },
                            {
                                throw TimeoutException("REQUEST_TIMED_OUT")
                            })
                        .bodyToMono(TResponse::class.java)
                        .timeout(Duration.ofMinutes(5)).log()
                } catch (ex: WebClientResponseException) {

                    if (ex.statusCode == HttpStatus.NOT_FOUND) {
                        return@flatMap Mono.error(NotFoundException("NOT_FOUND"))
                    }
                    if (ex.localizedMessage.contains("Error occurred while processing request")) {
                        return@flatMap Mono.error(InternalServerErrorException("ERROR_CALLING_IRIS"))
                    }

                    if (ex.localizedMessage.contains("is not deployed")) {
                        return@flatMap Mono.error(InternalServerErrorException("NOT_DEPLOYED"))
                    }

                    logger.error("[cbss] Errors\n${ex.responseBodyAsString}\n${ex.stackTraceToString()}")
                    return@flatMap Mono.error(InternalServerErrorException("SOMETHING_WENT_WRONG"))
                } catch (ex: Exception) {
                    logger.error("[cbss] Error\n${ex.localizedMessage}\n${ex.stackTraceToString()}")
                    logger.error("Error is ${ex::class.java} }")

                    if (ex.localizedMessage.startsWith("java.util.concurrent.TimeoutException")) {
                        return@flatMap Mono.error(TimeoutException("REQUEST_TIMED_OUT"))
                    }

                    return@flatMap Mono.error(InternalServerErrorException("SOMETHING_WENT_WRONG"))
                }
            }
    }

    inline fun <reified TResponse> queryCBSS(route: String): Mono<TResponse?> {
        initiator()
        return serviceInstanceFactory.getLoadBalancedUrl(baseUrl)
            .flatMap { it ->
                logger.info("-------------- Fetching $it/$route")
                try {
                    webClient
                        .get()
                        .uri("$it/$route")
                        .retrieve()
                        .onStatus({ httpStatus -> HttpStatus.NOT_FOUND == httpStatus },
                            { response ->
                                logger.error("=>[cbss] Error ${response.statusCode()}")
                                Mono.error<NotFoundException>(NotFoundException("NOT_FOUND"))
                            }
                        )
                        .onStatus({ httpStatus -> httpStatus.is5xxServerError },
                            {
                                Mono.error<InternalServerErrorException>(InternalServerErrorException("SOMETHING_WENT_WRONG"))

                            })
                        .bodyToMono(TResponse::class.java)
                        .onErrorResume {
                            return@onErrorResume Mono.error(it)
                        }
                        .timeout(Duration.ofMillis(timeout.toLong()))

                } catch (ex: WebClientResponseException) {
                    logger.error("=>[WebClientResponseException] Error ")
                    if (ex.statusCode == HttpStatus.NOT_FOUND) {
                        return@flatMap Mono.error(NotFoundException("NOT_FOUND"))
                    }

                    logger.error("[cbss] Error\n${ex.responseBodyAsString}\n${ex.stackTraceToString()}")
                    return@flatMap Mono.error(InternalServerErrorException("SOMETHING_WENT_WRONG"))
                } catch (ex: Exception) {
                    logger.error("[cbss] Error\n${ex.localizedMessage}\n${ex.stackTraceToString()}")

                    if (ex.localizedMessage.startsWith("java.util.concurrent.TimeoutException")) {
                        return@flatMap Mono.error(TimeoutException("REQUEST_TIMED_OUT"))
                    }

                    return@flatMap Mono.error(InternalServerErrorException("SOMETHING_WENT_WRONG"))
                }
            }
    }

    inline fun <reified TResponse> queryFluxFromCBSS(route: String): Flux<TResponse?> {
        initiator()

        return serviceInstanceFactory.getLoadBalancedUrl(baseUrl)
            .flatMapMany {
                logger.info("$it has come")
                try {
                    webClient
                        .get()
                        .uri("$it/$route")
                        .retrieve()
                        .onStatus({ httpStatus -> HttpStatus.NOT_FOUND == httpStatus },
                            { response ->
                                logger.error("=>[cbss] Error ${response.statusCode()}")
                                Mono.error<NotFoundException>(NotFoundException("NOT_FOUND"))
                            }
                        )
                        .onStatus({ httpStatus -> httpStatus.is5xxServerError },
                            {
                                Mono.error<InternalServerErrorException>(InternalServerErrorException("NOT_FOUND"))

                            })
                        .bodyToFlux(TResponse::class.java)
                        .onErrorResume { error ->
                            return@onErrorResume Flux.error(error)
                        }
                        .timeout(Duration.ofMillis(timeout.toLong())).log()

                } catch (ex: WebClientResponseException) {
                    if (ex.statusCode == HttpStatus.NOT_FOUND) {
                        return@flatMapMany Flux.error(NotFoundException("NOT_FOUND"))
                    }

                    logger.error("[cbss] Error\n${ex.responseBodyAsString}\n${ex.stackTraceToString()}")
                    return@flatMapMany Flux.error(InternalServerErrorException("SOMETHING_WENT_WRONG"))
                } catch (ex: Exception) {
                    logger.error("[cbss] Error\n${ex.localizedMessage}\n${ex.stackTraceToString()}")

                    if (ex.localizedMessage.startsWith("java.util.concurrent.TimeoutException")) {
                        return@flatMapMany Flux.error(TimeoutException("REQUEST_TIMED_OUT"))
                    }

                    return@flatMapMany Flux.error(InternalServerErrorException("SOMETHING_WENT_WRONG"))
                }
            }.onErrorContinue { t, _ ->
                logger.error("[cbss] Error\n${t.localizedMessage}\n${t.stackTraceToString()}")
                Flux.error<TResponse>(InternalServerErrorException("SOMETHING_WENT_WRONG"))
            }
    }
}