package rw.bk.notificationservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.r2dbc.config.EnableR2dbcAuditing
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.transaction.annotation.EnableTransactionManagement

@SpringBootApplication
@EnableTransactionManagement
@EnableR2dbcAuditing
@EnableR2dbcRepositories
@EnableKafka
class NotificationV2Application

fun main(args: Array<String>) {
    runApplication<NotificationV2Application>(*args)
}
