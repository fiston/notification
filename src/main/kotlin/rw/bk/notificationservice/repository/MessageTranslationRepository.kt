package rw.bk.notificationservice.repository

import org.reactivestreams.Publisher
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.relational.core.query.Criteria.where
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.bk.notificationservice.models.MessageTranslation
import java.util.*
import java.util.function.Function


interface MessageTranslationRepository:ReactiveCrudRepository<MessageTranslation,UUID> {

    fun findByNotificationMessagesTemplatesId(templateId:UUID): Flux<MessageTranslation>

    fun deleteAllByNotificationMessagesTemplatesId(templateId:UUID)
}