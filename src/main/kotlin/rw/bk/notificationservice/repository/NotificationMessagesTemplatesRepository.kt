package rw.bk.notificationservice.repository

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import rw.bk.notificationservice.models.NotificationMessagesTemplates
import java.util.UUID

interface NotificationMessagesTemplatesRepository:ReactiveCrudRepository<NotificationMessagesTemplates,UUID> {

    @Query("SELECT n.*, t.* FROM notification_messages_templates n LEFT JOIN message_translation t ON n.id = t.notification_messages_templates_id")
    fun findAllWithTranslations(): Flux<NotificationMessagesTemplates>

    fun findByMessageId(messageId:String): Mono<NotificationMessagesTemplates>
}