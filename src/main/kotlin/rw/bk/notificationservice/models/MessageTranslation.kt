package rw.bk.notificationservice.models

import org.springframework.data.annotation.*
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime
import java.util.*

@Table("message_translation")
data class MessageTranslation(
    @Id
    @get:JvmName("getId_")
    var id: UUID?=null,

    @Column("languages")
    val languages: String,

    @Column("message_content")
    val messageContent: String,

    @Column("notification_messages_templates_id")
    var notificationMessagesTemplatesId:UUID?=null,

    ) : Persistable<UUID> {
    @Transient
    var newMessageTranslation: Boolean = false
        private set

    override fun getId(): UUID? {
        return id?: UUID.randomUUID()
    }

    @Transient
    override fun isNew(): Boolean {
        return newMessageTranslation || this.id == null
    }

    fun setAsNew(): MessageTranslation {
        this.newMessageTranslation = true
        return this
    }

    @CreatedDate
    var createdDate: LocalDateTime = LocalDateTime.now()

    @LastModifiedDate
    var updatedDate: LocalDateTime = LocalDateTime.now()

    @Version
    private var version: Long = 0
}
