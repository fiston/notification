package rw.bk.notificationservice.models

import org.springframework.data.annotation.*
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime

import java.util.*

@Table(name = "notification_messages_templates")
data class NotificationMessagesTemplates(
    @Id
    @get:JvmName("getId_")
    var id: UUID? = null,

    @Column("message_id")
    var messageId: String,


) : Persistable<UUID> {

    @Transient
    var newNotificationMessage: Boolean = false
        private set

    override fun getId(): UUID {
        return id?: UUID.randomUUID()
    }

    @Transient
    override fun isNew(): Boolean {
        return newNotificationMessage || this.id == null
    }

    fun setAsNew(): NotificationMessagesTemplates {
        this.newNotificationMessage = true
        return this
    }

    override fun toString(): String {
        return "NotificationMessagesTemplates(" +
                "id=$id, " +
                "messageId='$messageId', " +
                "newNotificationMessage=$newNotificationMessage, " +
                "translations=$translations, " +
                "createdDate=$createdDate, " +
                "updatedDate=$updatedDate, " +
                "version=$version" +
                ")"
    }

    @Transient
    var translations: Set<MessageTranslation>?=null

    @CreatedDate
    var createdDate: LocalDateTime = LocalDateTime.now()

    @LastModifiedDate
    var updatedDate: LocalDateTime = LocalDateTime.now()

    @Version
    var version: Long = 0

}