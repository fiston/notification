package rw.bk.notificationservice.models.response

import rw.bk.notificationservice.helpers.jsonStringify


class ErrorResponse (
    var status:Int,
    var error: String,
    var message: String,
    var timestamps: String,
){
    override fun toString(): String {
        return this.jsonStringify()
    }
}