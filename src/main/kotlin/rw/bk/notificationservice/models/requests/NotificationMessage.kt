package rw.bk.notificationservice.models.requests

data class NotificationMessage(
    var arguments: ArrayList<String>? = null,
    var customerIdentityId: String? = null,
    var messageId: String,
)

