package rw.bk.notificationservice.models.requests

import jakarta.validation.constraints.NotBlank
import rw.bk.notificationservice.helpers.Language
import rw.bk.notificationservice.helpers.ValidEnum

class MessageTranslationRequest (

    @get:NotBlank(message="LANGUAGE CAN'T BE BLANK") @get:ValidEnum(enumClass = Language::class) val language: String,

    @get:NotBlank(message="TRANSLATION CAN'T BE BLANK") val translation: String,

    )
