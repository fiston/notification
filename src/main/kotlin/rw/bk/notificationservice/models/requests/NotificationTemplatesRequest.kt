package rw.bk.notificationservice.models.requests

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size

class NotificationTemplatesRequest (

    @get:NotNull(message="messageId CAN'T BE NULL") @get:NotBlank(message="messageId CAN'T BE BLANK") var messageId: String,

    @get:NotNull(message="TRANSLATION CAN'T BE NULL") @get:Size(min = 3, max = 3) var translations: Set<MessageTranslationRequest>
)