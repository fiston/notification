package rw.bk.notificationservice.exceptions


class InternalServerErrorException(message: String) : RuntimeException(message)

class NotFoundException(message: String) : RuntimeException(message)

class ServiceNotFoundException(message: String) : RuntimeException(message)

class BadRequestException(message: String) : RuntimeException(message)

class UnauthorizedException(message: String) : RuntimeException(message)

class TimeoutException(message: String) : RuntimeException(message)

class DuplicateReferenceException(message: String) : RuntimeException(message)

class NothingToSaveException(message: String) : RuntimeException(message)

class LoadingFromCbsException(message: String) : RuntimeException(message)

class CustomerOrAccountNotFoundException(message: String) : RuntimeException(message)

class DuplicateErrorMapping(
    var status:String?,
    override var message: String?
): RuntimeException(message)

data class ErrorResponsePayload(val status: Int, val error: String, val message: String, val timestamps: String)
