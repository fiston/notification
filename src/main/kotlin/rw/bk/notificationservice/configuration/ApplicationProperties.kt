package rw.bk.notificationservice.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class ApplicationProperties {

    @Value("\${integrations.kafka.broker-server}")
    val brokerServer: String = ""


    @Value(value = "\${integrations.kafka.broker-server}")
    val bootstrapAddress: String=""


    @Value(value = "\${integrations.kafka.group-id.notification}")
    val notificationGroupId: String=""

    @Value(value = "\${integrations.kafka.topic.notification}")
    val notificationTopic: String=""


}
