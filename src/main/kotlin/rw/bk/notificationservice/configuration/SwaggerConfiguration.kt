package rw.bk.notificationservice.configuration


import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import org.springdoc.core.models.GroupedOpenApi
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SwaggerConfig {
    @Bean
    fun customOpenAPI(@Value("\${springdoc.version}") appVersion: String?): OpenAPI? {
        return OpenAPI()
            .info(
                Info().title("Notification Service").version(appVersion)
                .license(License().name("BK All rights reserved").url("#")))
    }


    @Bean
    fun notificationOpenApi(): GroupedOpenApi {
        val paths = arrayOf("/**")

        return GroupedOpenApi.builder().group("Management").pathsToMatch(*paths)
            .build()
    }


}