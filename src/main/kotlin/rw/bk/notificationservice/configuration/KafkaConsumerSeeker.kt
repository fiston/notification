package rw.bk.notificationservice.configuration


import org.apache.kafka.common.TopicPartition
import org.springframework.kafka.listener.ConsumerSeekAware
import org.springframework.kafka.listener.ConsumerSeekAware.ConsumerSeekCallback
import org.springframework.stereotype.Component


@Component
class KafkaConsumerSeeker : ConsumerSeekAware {
    private lateinit var callback: ConsumerSeekCallback

    override fun onPartitionsAssigned(assignments: Map<TopicPartition, Long?>, callback: ConsumerSeekCallback) {
        this.callback = callback
        assignments.forEach { (topicPartition: TopicPartition, offset: Long?) ->
            if (topicPartition.topic().equals("notify-customer")) {
                callback.seekToBeginning(topicPartition.topic(), topicPartition.partition())
                callback.seek(topicPartition.topic(), topicPartition.partition(), offset!!)
            }
        }
    }

    override fun registerSeekCallback(callback: ConsumerSeekCallback) {
        this.callback = callback
    }
}
