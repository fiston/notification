package rw.bk.notificationservice.helpers

import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext

class EnumValidator : ConstraintValidator<ValidEnum, String> {
    private lateinit var validValues: Set<String>

    override fun initialize(constraintAnnotation: ValidEnum) {
        validValues = constraintAnnotation.enumClass.java.enumConstants.map { it.toString() }.toSet()
    }

    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        return value != null && validValues.contains(value)
    }
}
