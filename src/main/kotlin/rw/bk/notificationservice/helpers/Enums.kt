package rw.bk.notificationservice.helpers

enum class Language{
    ENGLISH, FRENCH, KINYARWANDA
}