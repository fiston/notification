package rw.bk.notificationservice.helpers

import org.slf4j.LoggerFactory
import org.springframework.web.reactive.function.client.ClientRequest
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.ExchangeFunction

class ApiServiceFilters {
    companion object {
        private val logger = LoggerFactory.getLogger(ApiServiceFilters::class.java)

        //Filters to log each api request and response
        fun log(appName: String): ExchangeFilterFunction {
            return ExchangeFilterFunction { request: ClientRequest, next: ExchangeFunction ->
                logger.info("$appName:  Performing ${request.method()} on ${request.url()} ")
                next.exchange(request).doOnNext { response: ClientResponse ->
                    logger.info(
                        "$appName: ${request.method()} : ${request.url()} - Response: ${response.statusCode()}"
                    )
                }
            }
        }

    }


}