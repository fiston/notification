package rw.bk.notificationservice.helpers

import com.fasterxml.jackson.databind.ObjectMapper
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

import java.util.*

//Function to use for debugging
fun Any.nyereka() = println(this.jsonStringify())

val objectMapper = ObjectMapper()

fun Any.jsonStringify(): String = objectMapper.writeValueAsString(this)

fun toMonoList(monoList: List<Mono<Any>>): Mono<List<Any>> {
    return Flux.zip(monoList) { array -> array.toList() }.singleOrEmpty()
}