package rw.bk.notificationservice.helpers

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.client.ServiceInstance
import org.springframework.cloud.client.loadbalancer.Response
import org.springframework.cloud.client.loadbalancer.reactive.ReactiveLoadBalancer
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import rw.bk.notificationservice.exceptions.ServiceNotFoundException
import java.util.function.Function

@Component
class ServiceInstanceFactory {

    @Autowired
    private lateinit var serviceInstanceFactory: ReactiveLoadBalancer.Factory<ServiceInstance>


    private val logger: Logger = LoggerFactory.getLogger(ServiceInstanceFactory::class.java)


    fun getLoadBalancedUrl(serviceHost: String): Mono<String> {

        logger.info("Getting URL for: $serviceHost ")

        val api = serviceInstanceFactory.getInstance(serviceHost)




        val chosen: Mono<Response<ServiceInstance>> = Mono.from(api.choose())

        val url = chosen
            .map(Function<Response<ServiceInstance>, String> { responseServiceInstance: Response<ServiceInstance> ->

                val server: ServiceInstance? = responseServiceInstance.server

                logger.info("Chosen server: ${server?.host} ")

                if (server == null)
                    throw ServiceNotFoundException("SERVICE_NOT_FOUND")

                val protocol: String = if (server.isSecure) "https://" else "http://"

                return@Function protocol + server.host + ':' + server.port
            }).onErrorContinue{t,_->
                logger.error("========= ${t.localizedMessage}")
            }.log()

        return url
    }
}