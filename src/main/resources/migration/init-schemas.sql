CREATE TABLE notification_messages_templates
(
     id           UUID PRIMARY KEY,
     message_id   VARCHAR(255) NOT NULL,
     created_date TIMESTAMP WITHOUT TIME ZONE,
     updated_date TIMESTAMP WITHOUT TIME ZONE,
     version      BIGINT NOT NULL
);

CREATE TABLE message_translation
(
     id                                 UUID PRIMARY KEY,
     languages                          VARCHAR(255) NOT NULL,
     message_content                    VARCHAR(255) NOT NULL,
     notification_messages_templates_id UUID NOT NULL,
     created_date                       TIMESTAMP WITHOUT TIME ZONE,
     updated_date                       TIMESTAMP WITHOUT TIME ZONE,
     version                            BIGINT NOT NULL,
     FOREIGN KEY (notification_messages_templates_id) REFERENCES notification_messages_templates(id)
);
